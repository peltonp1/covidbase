import roomgenerator as rg
import blockmesher as bm

class Shelf(rg.Box):

    L = 10.0 #length (x-dir)
    W = 0.5 #width  (y-dir)
    H = 2.5 #height (z-dir)
    
    spacebetween = 2.0 #currently both in x- and y-directions



    def __init__(self, x, y, n):
        
        xmin = x - 0.5 * Shelf.L
        ymin = y - 0.5 * Shelf.W
        zmin = 0.0
        xmax = x + 0.5 * Shelf.L
        ymax = y + 0.5 * Shelf.W
        zmax = Shelf.H

        p0 = rg.Point(xmin, ymin, zmin)
        p1 = rg.Point(xmax, ymax, zmax)

        rg.Box.__init__(self, p0, p1, id="aisle_{0}".format(n))

         


class Human:

    L = 0.4 #length (x-dir)
    W = 0.4 #width  (y-dir)
    H = 1.8 #height (z-dir)

    mouthD = 0.03
    

    def __init__(self, x, y, n):
        self.body = self.__create_body(x,y,n)
        self.mouth = self.__create_mouth(x,y,n)
        self.refinements = self.__create_refinements(x,y,n)

    def __create_body(self, x, y, n):
    
        #body
        xmin = x - 0.5*Human.L
        ymin = y - 0.5*Human.W
        zmin = 0.0
        xmax = x + 0.5*Human.L
        ymax = y + 0.5*Human.W
        zmax = Human.H

        
        p0 = rg.Point(xmin, ymin, zmin)
        p1 = rg.Point(xmax, ymax, zmax)
        box = rg.Box(p0, p1, id="human_body_{0}".format(n))
        return box

    def __create_mouth(self, x, y, n):

        xmin = x
        ymin = y - 0.5*Human.mouthD
        zmin = 1.65

        xmax = xmin + 0.5 * Human.L + Human.mouthD
        ymax = y + 0.5*Human.mouthD
        zmax = zmin + Human.mouthD

        p0 = rg.Point(xmin, ymin, zmin)
        p1 = rg.Point(xmax, ymax, zmax)
        box = rg.Box(p0, p1, id="human_mouth_{0}".format(n))
        return box
    

    def __create_refinements(self, x, y, n):

        refs = []
    
        xmin = x - 3.0
        xmax = x + 3.0
        ymin = y - 3.0
        ymax = y + 3.0
        zmin = -1
        zmax = 3.0

        p0 = rg.Point(xmin, ymin, zmin)
        p1 = rg.Point(xmax, ymax, zmax)
        box = rg.Box(p0, p1, id="refinement_body_{0}".format(n))

        refs.append(box)


        xmin = x - 1.5
        xmax = x + 1.5
        ymin = y - 1.5
        ymax = y + 1.5
        zmin = 1.5
        zmax = 2.0

        p0 = rg.Point(xmin, ymin, zmin)
        p1 = rg.Point(xmax, ymax, zmax)
        box = rg.Box(p0, p1, id="refinement_mouth_{0}".format(n))

        refs.append(box)

        return refs

    def write_stl(self, path):
        
        self.body.write_stl(path)
        self.mouth.write_stl(path)

        for ref in self.refinements:
            ref.write_stl(path)

    def get_bounding_box(self):
        return self.body

#THIS IS THE BASE SIZE FOR THE BACKGROUND GRID!
delta = 1*Human.W 


#NOTE IF YOU ADD MORE AISLES YOU NEED TO MANUALLY INCLUDE THE STL FILES IN SNAPPYHEXMESHDICT :(
nx_aisle = 1
ny_aisle = 4

def main():


    
    Lx= nx_aisle * Shelf.L + nx_aisle*Shelf.spacebetween
    Ly= ny_aisle * Shelf.W + ny_aisle*Shelf.spacebetween 

    Lz = 5.0

    room = rg.Room(Lx=Lx, Ly=Ly, Lz=Lz)

    create_aisles(room, nx_aisle, ny_aisle)

    #add a single human to x = y = 0
    human = Human(0, 0, 0)
    room.add_obstacle(human)
    
    #create a refinement region for z < 3.5
    box = rg.Box(rg.Point(-1000, -1000, -1), rg.Point(1000, 1000, 3.5), id="refinement_large_0")
    box.write_stl("./cough/constant/triSurface/")
    box.write_stl("./create_turbulence/constant/triSurface/")


    room.write_stl("./cough/constant/triSurface/")
    room.write_stl("./create_turbulence/constant/triSurface/")
    create_background_blockmesh(room)
    

    #room.plot_floorplan()

def create_background_blockmesh(room):

    
    

    xmin = room.p0.x
    xmax = room.p1.x
    ymin = room.p0.y
    ymax = room.p1.y
    zmin = room.p0.z
    zmax = room.p1.z

    cell_scale = 1
    nx = int((xmax - xmin)/delta) * cell_scale
    ny = int((ymax - ymin)/delta) * cell_scale
    nz = int((zmax - zmin)/delta) * cell_scale


    print ("TOTAL CELL COUNT {0}".format(nx * ny * nz))

    bm.create_box(xmin, xmax, ymin, ymax, zmin, zmax, opath="./create_turbulence/system/blockMeshDict", nx=nx, ny=ny, nz=nz)
    bm.create_box(xmin, xmax, ymin, ymax, zmin, zmax, opath="./cough/system/blockMeshDict", nx=nx, ny=ny, nz=nz)
    

def create_aisles(room, nx, ny):

    dx = Shelf.L + Shelf.spacebetween
    dy = Shelf.W + Shelf.spacebetween

    n = 0
    for i in range(nx):
        for j in range(ny):
            #Note! 0.5dx/dy shift due to periodicity
            x = ( -0.5 * room.Lx + 0.5 * dx) + i * dx 
            y = ( -0.5 * room.Ly + 0.5 * dy) + j * dy 
            room.add_obstacle(Shelf(x,y,n))
            n+=1
    

def create_human(room, x, y, n):

    xmin = x - 0.5*Human.L
    ymin = y - 0.5*Human.W
    zmin = 0.0
    xmax = x + 0.5*Human.L
    ymax = y + 0.5*Human.W
    zmax = Human.H

    
    p0 = rg.Point(xmin, ymin, zmin)
    p1 = rg.Point(xmax, ymax, zmax)
    box = rg.Box(p0, p1, id="human_{0}".format(n))
    room.add_obstacle(box)



main()


